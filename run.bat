@echo off

dir /s /B *.java > sources.txt
javac @sources.txt

for %%f in (input*.txt) do (
  java -cp . Main %%f
  echo.
)