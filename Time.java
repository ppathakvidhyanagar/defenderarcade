import java.time.LocalTime;

public class Time {
	
	private LocalTime start;
	
	private LocalTime end;	
	

	public Time(LocalTime start, LocalTime end) {
		super();
		this.start = start;
		this.end = end;
	}

	/**
	 * @return the start
	 */
	public LocalTime getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(LocalTime start) {
		this.start = start;
	}

	/**
	 * @return the end
	 */
	public LocalTime getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(LocalTime end) {
		this.end = end;
	}
	
	
}
