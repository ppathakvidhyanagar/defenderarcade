import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Pranav Pathak
 *
 */
public class DefenderArcade {
  
	/**
	 * @param start
	 * @param end
	 * @param slots
	 * @return
	 * 
	 * Find number of conflicted slots with ${start} and ${end} 
	 */
	private int countConflictedCommonSlots(LocalTime start, LocalTime end, List<Time> slots) {
		int busyMachine=0;
		
		for (Time s : slots) {
			Boolean isBusy= ((start.equals(s.getStart()) || start.isAfter(s.getStart())) 
					&& (end.equals(s.getEnd()) || end.isBefore(s.getEnd())));
			if(isBusy) {
				busyMachine++;
			}
		}
		return busyMachine;
	}
	
	public int countArcades(List<String> times) {
		
		// parse time and sort it by start date
		List<Time> slots = times.stream().map(time -> {
			String startS= time.split(" ")[0];
			String endS= time.split(" ")[1];

			LocalTime start = LocalTime.of(Integer.parseInt(startS.substring(0,startS.length()-2)), Integer.parseInt(startS.substring(startS.length()-2)));
			LocalTime end = LocalTime.of(Integer.parseInt(endS.substring(0,endS.length()-2)), Integer.parseInt(endS.substring(endS.length()-2)));
			return new Time(start, end);
		}).sorted(new Comparator<Time>() {
			@Override
			public int compare(Time o1, Time o2) {
				
				return o1.getStart().compareTo(o2.getStart());
			}
		}).collect(Collectors.toList());
		
		
		LocalTime tempStart = null;
		LocalTime tempEnd = null;
		
		int minimumRequiredMachine = 0;
		
		/**
		 * Algo to find common slots.
		 * 
		 * 1. Iterate parse timings;
		 * 2. Check ${tempStart} is null or not. If true then goto 2nd step else goto 3rd step.
		 * 3. Set ${tempStart} and ${tempEnd} with ${start} and ${end} 
		 * 	 note: ${start} and ${end} refere current values with each iteration.
		 * 4. Check ${start} is before ${tempEnd} if true then goto 5th step else goto 6th step.
		 * 5. Replace ${tempEnd} with ${end} if ${end} is before ${$tempEnd}, 
		 * 		and Replace ${tempStart} with ${start} if ${start} is after ${$tempStart}. Goto 7th step.
		 * 6. Calculate number of conflicted slots by ${tempEnd} and ${tempStart}. Mark ${tempStart} and ${tempEnd}.
		 * 7. Repeat 4 steps unitl iteration finished.
		 */
		//Step 1
		for (int i = 0; i < slots.size(); i++) {
			Time slot = slots.get(i);
			
			//Step 2
			if(tempStart == null) {
				tempStart = slot.getStart();
				tempEnd = slot.getEnd();
				continue;
			}
			//Step 3
			if(slot.getStart().isBefore(tempEnd)) {
				//Step 5
				if(slot.getEnd().isBefore(tempEnd)) {
					tempEnd = slot.getEnd();
				}
				if(slot.getStart().isAfter(tempStart)) {
					tempStart= slot.getStart();
				}
			}else {
				//Step 6
				int conflictedSlots = countConflictedCommonSlots(tempStart, tempEnd, slots);
				
				if(minimumRequiredMachine< conflictedSlots) {
					minimumRequiredMachine = conflictedSlots;
				}
				
				tempStart = slot.getStart();
				tempEnd = slot.getEnd();
			}
			
		}
		
		return minimumRequiredMachine;
	  }
}
